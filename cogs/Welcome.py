import discord
from util import checks
from discord.ext import commands
from discord.ext.commands import Cog

class Welcome(Cog, command_attrs=dict(hidden=True)):
    """
    Automatically welcome new people!
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['welcome', 'wc'])
    @checks.is_admin()
    async def set_welcome_channel(self, ctx):
        """
        This command sets the channel to be used to welcome members in.
        """
        guild = ctx.guild.id
        channel = ctx.channel.id
        self.bot.db.add(f'{guild}::welcome_channel', [channel])
        return await ctx.send(f'{ctx.channel.mention} has been set as the welcome channel for `{ctx.guild.name}`.')

    @commands.command(aliases=['delete_wc','wcd'])
    @checks.is_admin()
    async def unset_welcome_channel(self, ctx):
        """
        This command unsets the welcome channel for the server.
            Note: This command can be run from any channel.
        """
        guild = ctx.guild.id
        self.bot.db.delete(f'{guild}::welcome_channel', [])
        return await ctx.send(f'Unset welcome channel for `{ctx.guild.name}`')

    @commands.command(aliases=['wmessage', 'wm'])
    @checks.is_admin()
    async def set_welcome_message(self, ctx, *, message):
        """
        This command sets the welcome message to be displayed when someone new joins.

        If you would like to mention the user in the welcome message include {mention} (with the brackets)
        in the message.
        To mention a channel follow this format <#channel_id> (replace "channel_id" with the id of the channel you would like to mention.)
        """
        guild = ctx.guild.id
        self.bot.db.add(f'{guild}::welcome_message', [message])
        return await ctx.send(f'`{message}` set as welcome message for `{ctx.guild.name}`')

    @commands.command(aliases=['wmessage_delete','wmd'])
    @checks.is_admin()
    async def unset_welcome_message(self, ctx):
        """
        This command unsets the welcome message for the current server.
        """
        guild = ctx.guild.id
        self.bot.db.delete(f'{guild}::welcome_message', [])
        return await ctx.send(f'Unset welcome message for `{ctx.guild.name}`')


    @Cog.listener()
    async def on_member_join(self, member: discord.Member):
        """
        Sends the guild welcome message to a new member when they join.
        """
        guild = member.guild.id
        channel_id = self.bot.cache.get(f'{guild}::welcome_channel')[0]['channel_id']
        message = self.bot.cache.get(f'{guild}::welcome_message')[0]['message']

        fmt = {'mention': member.mention}
        formatted_msg = message.format(**fmt)

        channel = member.guild.get_channel(channel_id)
        return await channel.send(formatted_msg)


def setup(authbot):
    authbot.add_cog(Welcome(authbot))

def teardown(authbot):
    authbot.remove_cog(Welcome)