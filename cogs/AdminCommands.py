import discord
from util import checks
from discord.ext.commands import Cog
from discord.ext import commands

class AdminCommands(Cog, name="Admin Commands", command_attrs=dict(hidden=True)):
    def __init__(self, bot):
        self.bot = bot
        self.db = self.bot.db
        self.cache = self.bot.cache
        self.logger = self.bot.logger

    @commands.command(aliases=['l'])
    @commands.is_owner()
    async def load_cog(self, ctx, cog):
        """
        Loads a cog into to bot.
        :param cog: the name of the cog. (case sensitive)
        """
        self.logger.info(f"{ctx.author.name}#{ctx.author.discriminator} attempted to load cog {cog}.")
        try:
            self.bot.load_extension(f'cogs.{cog}')
        except Exception as e:
            self.logger.fatal(f"Cog {cog} failed to load.")
            self.logger.fatal(e)
            return await ctx.send(f"Failed to load {cog}. ```{e}```")
        else:
            self.logger.info(f"Cog {cog} successfully unloaded.")
            return await ctx.send(f"Loaded {cog}.")

    @commands.command(aliases=['ul'])
    @commands.is_owner()
    async def unload_cog(self, ctx, cog):
        """
        Unloads a cog from the bot.
        :param cog: the name of the cog. (case sensitive)
        """
        cogs = self.bot.cogs
        self.logger.info(f"{ctx.author.name}#{ctx.author.discriminator} attempted to unload cog {cog}")
        try:
            if cog not in cogs:
                raise ValueError("Cog not in bot cog list.")
            self.bot.unload_extension(f'cogs.{cog}')
        except Exception as e:
            self.logger.fatal(f"Error unloading cog {cog}.")
            self.logger.fatal(e)
            return await ctx.send(f"Error unloading {cog}. ```{e}```")
        else:
            self.logger.info(f"Cpg {cog} successfully unloaded.")
            return await ctx.send(f"Unloaded {cog}.")

    @commands.command(aliases=['ld'])
    @commands.is_owner()
    async def loaded(self, ctx):
        """
        Shows the currently loaded cogs and extensions.
        """
        exts = "\n".join(self.bot.extensions)
        cogs = "\n".join(self.bot.cogs)

        ext_num = len(self.bot.extensions)
        cog_num = len(self.bot.cogs)

        return await ctx.send(f'```\n'
                              f'Extensions: {ext_num} Extensions Loaded with {cog_num} Cogs \n\n{exts} \n\n'
                              f'Cogs: {cog_num} Loaded \n\n{cogs}'
                              f'```')

    @commands.command(aliases=['ad'])
    @checks.guild_owner()
    async def admin(self, ctx):
        """
        Adds every user mentioned to the list of admin users.
        """
        users = ctx.message.mentions

        if len(users) == 0:
            return await ctx.send(f"No users mentioned passed to command. Were you trying to add a role? If so, use the `{self.bot.prefix}admin_role` command.")

        new = []
        failed = []
        msg = ""
        for user in users:
            try:
                self.db.add(f"{ctx.guild.id}::admin_users", {'user_id': user.id})
                self.logger.info(f"{ctx.author.name}#{ctx.author.discriminator} added {user.name}#{user.discriminator} to the list of admin users.")
                new.append(user.display_name)
            except Exception as e:
                self.logger.error(f"Error adding {user.name}#{user.discriminator} to the list of admin users.")
                self.logger.error(e)
                failed.append(user.display_name)
        if len(new) != 0:
            new = '\n'.join(new)
            msg += f"The following users have been added to the admin list: ```{new}```\n"
        if len(failed) != 0:
            failed = '\n'.join(failed)
            msg += f"The following users have not been added to the admin list: ```{failed}```"
        return await ctx.send(msg)

    @commands.command(aliases=['rad'])
    @checks.guild_owner()
    async def remove_admin(self, ctx):
        """
        Removes the user(s) mentioned from the admin users list.
        """
        users = ctx.message.mentions

        if len(users) == 0:
            return await ctx.send(f"No users mentioned passed to command. Were you trying to remove a role? If so, use the `{self.bot.prefix}remove_admin_role` command.")

        failed = []
        rem = []
        msg = ""
        for user in users:
            try:
                self.db.delete(f"{ctx.guild.id}::admin_users", {'user_id': user.id})
                self.logger.info(f"{ctx.author.name}#{ctx.author.discriminator} removed {user.name}#{user.discriminator} from the list of admin users")
                rem.append(user.display_name)
            except Exception as e:
                self.logger.error(f"Error removing {user.name}#{user.discriminator} from the list of admin users.")
                self.logger.error(e)
                failed.append(user.display_name)
        
        if len(rem) != 0:
            rem = '\n'.join(rem)
            msg += f"The following users have been removed from the list of admin users: ```{rem}```"
        if len(failed) != 0:
            failed = '\n'.join(failed)
            msg += f"The following users have not been removed from the admin user list: ```{failed}```"

        return await ctx.send(msg) 

    @commands.command(aliases=['ar'])
    @checks.guild_owner()
    async def admin_role(self, ctx):
        """
        Adds mentioned role(s) to the list of admin roles.
        """
        roles = ctx.message.role_mentions

        if len(roles) == 0:
            return await ctx.send(f"No roles mentioned passed to command. Were you trying to add a user? If so, use the `{self.bot.prefix}admin` command.")

        new = []
        failed = []
        msg =""
        for role in roles:
            try:
                self.db.add(f"{ctx.guild.id}::admin_roles", {'role_id': role.id})
                self.logger.info(f"{role.name} added to the admin roles list by {ctx.author.name}#{ctx.author.discriminator}")
                new.append(role.name)
            except Exception as e:
                self.logger.error(f"Error adding {role.name} to the list of admin roles.")
                self.logger.error(e)
                failed.append(role.name)
        if len(new) != 0:
            new = '\n'.join(new)
            msg += f"The following roles have been added to the admin list: ```{new}```\n"
        if len(failed) != 0:
            failed = '\n'.join(failed)
            msg += f"The following roles have not been added to the admin list: ```{failed}```"
        return await ctx.send(msg)

    @commands.command(aliases=['rar'])
    @checks.guild_owner()
    async def remove_admin_role(self, ctx):
        """
        Removes mentioned role(s) from the list of admin roles.
        """
        roles = ctx.message.role_mentions

        if len(roles) == 0:
            return await ctx.send(f"No roles mentioned passed to command. Were you trying to remove a user? If so, use the `{self.bot.prefix}remove_admin` command.")

        rem = []
        failed = []
        msg =""
        for role in roles:
            try:
                self.db.delete(f"{ctx.guild.id}::admin_roles", {'role_id': role.id})
                self.logger.info(f"{role.name} removed from the admin roles list by {ctx.author.name}#{ctx.author.discriminator}")
                rem.append(role.name)
            except Exception as e:
                self.logger.error(f"Error removing {role.name} from the list of admin roles.")
                self.logger.error(e)
                failed.append(role.name)
        if len(rem) != 0:
            rem = '\n'.join(rem)
            msg += f"The following roles have been removed from the admin list: ```{rem}```\n"
        if len(failed) != 0:
            failed = '\n'.join(failed)
            msg += f"The following roles have not been removed from the admin list: ```{failed}```"
        return await ctx.send(msg)

    @commands.command(aliases=['cstats'], hidden=True)
    @checks.guild_owner()
    async def cache_stats(self, ctx):
        stat = self.cache.cache.get_stats()
        stats = stat[0][1]
        maxmib = float(stats["limit_maxbytes"])/1048576
        msg = f'```' \
              f'Memcache Stats: \n-------------------\n' \
              f'Up Time (in seconds): {stats["uptime"]}\n' \
              f'Connections: Current: {stats["curr_connections"]} | Total: {stats["total_connections"]}\n' \
              f'Threads: {stats["threads"]}\n' \
              f'Max Size: {stats["limit_maxbytes"]} bytes | {str(maxmib)} MiB\n' \
              f'Total Size: {stats["bytes"]} bytes \n' \
              f'Cache Items: Current: {stats["curr_items"]} | Total: {stats["total_items"]}\n\n' \
              f'Hits and Misses:\n-------------------\n' \
              f'Get: {stats["get_hits"]} / {stats["get_misses"]}\n' \
              f'Del: {stats["delete_hits"]} / {stats["delete_misses"]}\n' \
              f'```'
        return await ctx.send(msg)

    @commands.command(aliases=['lad'], hidden=True)
    @checks.is_admin()
    async def list_admin(self, ctx):
        """
        Lists all of the admin users and roles for the current guild.
        """
        users = self.cache.get(f"{ctx.guild.id}::admin_users")
        roles = self.cache.get(f"{ctx.guild.id}::admin_roles")
        a_users = []
        a_roles = []
        for user in users:
            admin = ctx.guild.get_member(user_id=user['user_id'])
            if admin is None:
                a_users.append(f"<@{user['user_id']}>")
            else:
                a_users.append(f"{admin.display_name} ({admin.name}#{admin.discriminator})")
        for role in roles:
            admin = ctx.guild.get_role(role_id=role['role_id'])
            if role is None:
                a_roles.append(f"<Role:{role['role_id']}>")
            else:
                a_roles.append(f"{admin.name} (id: {admin.id})")

        a_users = "\n".join(a_users)
        a_roles = "\n".join(a_roles)

        msg = f"The following users are currently bot admins for `{ctx.guild.name}`:\n```{a_users}```\n"\
              f"The following roles are currently bot admins for `{ctx.guild.name}`:\n```{a_roles}```\n"

        return await ctx.send(msg)


def setup(aabot):
    aabot.add_cog(AdminCommands(aabot))


def teardown(aabot):
    aabot.remove_cog(AdminCommands)