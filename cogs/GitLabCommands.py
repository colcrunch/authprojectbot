import discord
from discord.ext import commands
from discord.ext.commands import Cog
import checks


class GitLabCommands(Cog, name='Git Lab Commands'):
    def __init__(self, bot):
        self.bot = bot
        self.db = bot.db
        self.cache = bot.cache
    
    @commands.command(aliases=['np'])
    @checks.is_admin()
    async def new_project(self, ctx, name, short_name, project_id, origin=False):
        pass


def setup(aabot):
    aabot.add_cog(GitLabCommands(aabot))


def teardown(aabot):
    aabot.remove_cog(GitLabCommands)
