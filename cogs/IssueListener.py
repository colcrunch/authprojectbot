import discord
from discord.ext import commands
from discord.ext.commands import Cog


class IssueListener(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db = bot.db
        self.cache = bot.cache
        self.logger = bot.logger

    def __cog_keys(self):
        """
        This function loads information into cog-specific cache keys. 
        """
        for guild in self.bot.guilds:
            projects = self.cache.get(f"{guild}::projects")
            short_names = self.cache.cache.set(f"{guild}::projects/short_names", [project['short_name'] for project in projects])
            names = self.cache.cache.set(f"{guild}::projects/names", [project['name'] for project in projects])

    pass


def setup(aabot):
    aabot.add_cog(IssueListener(aabot))


def teardown(aabot):
    aabot.remove_cog(IssueListener)