import discord
import logging
import os
import datetime
if os.path.exists('config.py'):
    import config
from discord.ext.commands import Bot
from util.db import BotCache, BotDB

log_level = "DEBUG"
logger = None

if os.path.exists("config.py"):
    log_level = config.LOG_LEVEL

if os.path.exists('logs'):
    level = log_level
    logger = logging.getLogger('discord')
    logger.setLevel(level)
    handler = logging.FileHandler(filename=f'logs/discord{datetime.datetime.utcnow().strftime("_%Y-%m-%d_%H%M")}.log',
                                  encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s [%(name)s] %(message)s'))
    logger.addHandler(handler)


class ProjectBot(Bot):
    def __init__(self, *args, **kwargs):
        self.token = config.BOT_TOKEN
        self.prefix = config.PREFIX
        self.playing = config.MSG
        self.description = "Project Bot is a bot written in py3 for interacting with GitLab."

        self.start_time = datetime.datetime.utcnow()

        self.logger = logger
        self.db = BotDB('db/bot.db', self)
        self.cache = BotCache(self)

        super().__init__(command_prefix=self.prefix, description=self.description, pm_help=None, *args, **kwargs)
    
    def run(self):
        super().run(self.token)

    async def on_ready(self):
        # Load Cogs
        for cog in config.COGS:
            try:
                self.load_extension(f'cogs.{cog}')
            except Exception as e:
                logger.fatal(f'{cog} failed to load. Exception: ')
                logger.fatal(e)
                print(f"{cog} failed to load")
            else:
                logger.debug(f"{cog} loaded.")
                print(f"{cog} loaded")

        await self.change_presence(activity=discord.Game(name=self.playing))
        print("\nLogged In")
        print(self.user.name)
        print(self.user.id)
        print("--------------")