from bot import ProjectBot as aabot
import os
import shutil
import sys

if os.path.exists('config.py'):
    def main():
        bot = aabot()
        aabot.run(bot)
else:
    def main():
        print('No config found!\n'
              ' Please run the makeconfig command, then fill out config.py.')


def makeconfig():
    print('Making config file.')
    shutil.copy('config.py.example', 'config.py')

    return print('Please edit config.py to configure your bot.')

def migrate():
    bot = aabot()
    bot.db.update_db()
    return

if __name__ == '__main__':
    if len(sys.argv) == 1:
        main()
    if sys.argv[1] == 'makeconfig':
        makeconfig()
    elif sys.argv[1] == 'migrate':
        migrate()
    else:
        print(sys.argv[1]+' is not a valid argument. Starting bot.')
        main()
