import memcache
import sqlite3
import yaml


class DBConn:
    def __init__(self, dbString):
        self.dbName = dbString
        
    def __get_col_names(self, cursor):
        """
        Returns a list of column names for the table being queried.
        """
        return list(map(lambda x: x[0], cursor.description))

    def __open(self):
        """
        Connects to the database and returns a cursor object.
        """
        self.conn = sqlite3.connect(self.dbName, detect_types=sqlite3.PARSE_COLNAMES)
        return self.conn.cursor()
    
    def __close(self):
        """
        Closes the database connection
        """
        return self.conn.close()

    def execute_all(self, stmt):
        """
        Executes the query, and returns all results.
        Note: Using this method with queries that do not return results (Such as CREATE, DELETE, or INSERT)
              will likely cause errors.
        :param stmt: query String
        :return: A dictionary containing all rows returned with column names for easy access.
        """
        c = self.__open()
        t = c.execute(stmt)
        col_names = self.__get_col_names(c)
        all = t.fetchall()
        named_result = []
        for row in all:
            named = {}
            for idx, col in enumerate(col_names):
                named[col] = row[idx]
            named_result.append(named)
        self.conn.commit()
        self.__close()
        return named_result

    def execute_one(self, stmt):
        """
        Executes the query, and returns the first result.
        Note: Using this method with queries that do not return results (Such as CREATE, DELETE, or INSERT)
              will likely cause errors.
        :param stmt: query String
        :return: A dictionary containing the first returned returned with column names for easy access.
        """
        c = self.__open()
        t = c.execute(stmt)
        col_names = self.__get_col_names(c)
        one = t.fetchone()
        named_result = {}
        for idx, col in enumerate(col_names):
            named_result[col] = one[idx]
        self.conn.commit()
        self.__close()
        return named_result

    def execute_commit(self, stmt):
        """
        Executes a query that makes changes that need to be committed, and do not return any results. (Such as CREATE or DELETE)
        :param stmt: query String
        """
        c = self.__open()
        c.execute(stmt)
        self.conn.commit()
        self.__close()
        return

class BotDB:
    def __init__(self, db, bot):
        self.db = DBConn(db)
        self.bot_tables = self.__load_table_defs()
        self.bot = bot
        self.logger = bot.logger

    def __load_table_defs(self):
        """
        Loads table definitions from a yml file that describes all actions that might be performed on the table.
        """
        with open('db/tables.yml', 'r') as stream:
            try:
                defs = yaml.safe_load(stream)
            except yaml.YAMLError as e:
                self.logger.fatal("Error loading table definitions!")
                self.logger.fatal(e)
        return defs
    
    def __get_tables(self):
        """
        Gets a list of existing tables from the db.
        """
        qString = "SELECT name FROM sqlite_master WHERE type = 'table'"
        res = self.db.execute_all(qString)
        tables = []
        for row in res:
            tables.append(row['name'])
        return tables

    def __create_table(self, table_query):
        """
        Creates a table in the database.
        """
        self.db.execute_commit(table_query)
        return

    def __stringify_values(self, values: dict):
        return {k: (str(x) if type(x) is not str else f'"{x}"') for k, x in values.items()}

    def update_db(self):
        """
        Update the database.
            TODO: Make this function handle changes to table fields.
        """
        tables = self.__get_tables()
        for table in self.bot_tables:
            if table not in tables:
                self.logger.info(f"Creating {table} in bot database.")
                self.__create_table(self.bot_tables[table]['create'])
        return
    
    def _get_cache(self, key, value):
        """
        Gets data to be loaded into the cache.
            Note: This function should NEVER be called by any entity other than that of
            the BotCache class.
        
        :param key: the cache key to get data for
        :param value: the variable part of the cache key
        :return: all data returned from the database.
        """
        if key not in self.bot_tables:
            raise ValueError(f"Table {key} does not exist in the database.")
        if 'cache' not in self.bot_tables[key]:
            raise ValueError(f"No caching instructions found for {key}")

        return self.db.execute_all(self.bot_tables[key]['cache'].format(value))

    def add(self, key, values: dict):
        """
        Adds data to the database for a specified cache key.

        :param key: the cache key to add data to. (This represents the FULL key)
        :param values: a dict of the values to add to the database.
        :return:
        """
        var, key_name = self.bot.cache._parse_key(key)

        if key_name not in self.bot_tables:
            raise ValueError(f"Table {key_name} does not exist in the database.")
        if 'add' not in self.bot_tables[key_name]:
            raise ValueError(f"No value adding instructions found for {key}")
        
        values = self.__stringify_values(values)
        self.db.execute_commit(self.bot_tables[key_name]['add'].format(var, **values))
        self.bot.cache.load_key(key)
        return

    def delete(self, key, values: dict):
        """
        Deletes data from the database for a specified cache key.

        :param key: the cache key to delete data from. (This represents the FULL key)
        :param values: a dict of the values to delete from the database.
        :return:
        """
        var, key_name = self.bot.cache._parse_key(key)

        if key_name not in self.bot_tables:
            raise ValueError(f"Table {key_name} does not exist in the database.")
        if 'delete' not in self.bot_tables[key_name]:
            raise ValueError(f"No value deletion instructions found for {key}")
        
        values = self.__stringify_values(values)
        self.db.execute_commit(self.bot_tables[key_name]['delete'].format(var, **values))
        self.bot.cache.load_key(key)
        return


class BotCache:
    def __init__(self, bot):
        self.cache = memcache.Client(['127.0.0.1:11211'], debug=1)
        self.db = bot.db
        self.bot = bot
        self.logger = bot.logger

    def _parse_key(self, key):
        """
        Parses a cache key into its constituent parts.

        :return: a list representing [variable, key_name]
        """
        return key.split("::")

    def load_key(self, key):
        """
        Loads data from the database into the cache for a given key.

        :param key: the full cache key to load.
        :return:
        """
        var, key_name = self._parse_key(key)
        values = self.db._get_cache(key_name, var)
        self.cache.set(f'{var}::{key_name}', values)
        return

    def get(self, key):
        """
        Gets data from the cache for a given key.

        If the key is empty, we attempt to fill it.

        :param key: the full cache key.
        :return: the cached data
        """
        val = self.cache.get(key)
        if val is None:
            self.load_key(key)
            val = self.cache.get(key)
        return val
