import discord
from discord.ext import commands
from bot import ProjectBot as Bot

def is_admin():
    """ Checks if a user is on the list of admins for a guild. """
    async def predicate(ctx):
        if type(ctx.channel) is discord.DMChannel:
            return False
        if await ctx.bot.is_owner(ctx.author):
            return True
        elif ctx.author is ctx.guild.owner:
            return True
        sid = ctx.guild.id
        user = ctx.author.id
        roles = ctx.author.roles
        admin_users = ctx.bot.cache.get(f'{sid}::admin_users')
        admin_roles = ctx.bot.cache.get(f'{sid}::admin_roles')
        
        admin = user in admin_users
        if admin == False:
            for role in roles:
                if role.id in admin_roles:
                    admin = True
        return admin
    return commands.check(predicate)


def guild_owner():
    """ Checks if a user is the owner of the guild. """
    async def predicate(ctx):
        if type(ctx.channel) is discord.DMChannel:
            return False
        if await ctx.bot.is_owner(ctx.author):
            return True
        if ctx.author is ctx.guild.owner:
            return True
    return commands.check(predicate)
