from graphql_client import GraphQLClient
from gql_query_builder import GqlQuery
from bot import logger

class GitLabQL:
    def __init__(self, project_id):
        self.ws = GraphQLClient('ws://gitlab.com/api/graphql')

        self.project = project_id

    def __subscribe(self, query, callback, variables: dict = None):
        if variables:

            if type(variables) is not dict:
                raise TypeError(f"Expected variables to be of type {type(dict())}, got {type(variables)} instead.")

            sub_id = self.ws.subscribe(query, variables=variables, callback=callback)
        else:
            sub_id = self.ws.subscribe(query, callback=callback)
        return sub_id

    def subscribe(self, query: GqlQuery, callback):
        subQ = f"""subscription{{
            {query}
            }}"""
        sub_id = self.__subscribe(subQ, callback)
        logger.debug(sub_id)
        return sub_id
